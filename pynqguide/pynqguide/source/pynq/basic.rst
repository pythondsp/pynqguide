Pynq
****



Basic designs
=============

In this section, two designs are tested on the Pynq board. The designs are taken from `FPGA designs with VHDL <https://vhdlguide.readthedocs.io/en/latest/index.html>`_, 


Half adder
----------

Half adder is implemented in this design. Toggle the switch to see the output at the LEDs. 

.. code-block:: vhdl
    :linenos:

    -- half_adder.vhd

    library ieee;
    use ieee.std_logic_1164.all;

    entity half_adder is
    port(
        a, b : in std_logic;
        sum, carry : out std_logic
    );
    end half_adder;


    architecture arch of half_adder is
    begin
        sum <= a xor b;
        carry <= a and b;
    end arch; 
        


.. code-block:: text
    :linenos:


    # half_adder.xdc 

    ### a and b to switch
    set_property -dict { PACKAGE_PIN M20   IOSTANDARD LVCMOS33 } [get_ports { a }]; #IO_L7N_T1_AD2N_35 Sch=sw[0]
    set_property -dict { PACKAGE_PIN M19   IOSTANDARD LVCMOS33 } [get_ports { b }]; #IO_L7P_T1_AD2P_35 Sch=sw[1]


    ### sum and carry to LED
    set_property -dict { PACKAGE_PIN R14   IOSTANDARD LVCMOS33 } [get_ports { sum }]; #IO_L6N_T0_VREF_34 Sch=led[0]
    set_property -dict { PACKAGE_PIN P14   IOSTANDARD LVCMOS33 } [get_ports { carry }]; #IO_L6P_T0_34 Sch=led[1]




Mod-m counter
-------------

Here Mod-m counter is implemented which is described `here <https://vhdlguide.readthedocs.io/en/latest/vhdl/vvd.html#mod-m-counter>`_,

.. code-block:: vhdl
    :linenos:

    -- modMCounter_VisualTest.vhd

    library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

    entity modMCounter_VisualTest is 
        generic (
                M : integer := 12; -- count from 0 to M-1
                N : integer := 4   -- N bits required to count upto M i.e. 2**N >= M
        );
        port(
            CLOCK_50, reset: in std_logic;
            LEDR : out std_logic_vector (1 downto 0);
            LEDG : out std_logic_vector(N-1 downto 0)
        );
    end modMCounter_VisualTest;

    architecture arch of modMCounter_VisualTest is
        signal clk_Pulse1s: std_logic;
        signal count : std_logic_vector(N-1 downto 0);
    begin
        -- clock 1 s
        clock_1s: entity work.clockTick
        generic map (M=>50000000, N=>26)
        port map (clk=>CLOCK_50, reset=>reset, 
                        clkPulse=>clk_Pulse1s);
                        
        LEDR(0) <= clk_Pulse1s; -- display clock pulse of 1 s
        
        -- modMCounter with 1 sec clock pulse
        modMCounter1s: entity work.modMCounter
        generic map (M=>M, N=>N)
        port map (clk=>clk_Pulse1s, reset=>reset, 
                        complete_tick=>LEDR(1),count=>count);
                        
        LEDG <= count; -- display count on green LEDs
    end;



.. code-block:: vhdl
    :linenos:

    -- clockTick.vhd

    library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

    entity clockTick is 
        -- M = 5000000, N = 23 for 0.1 s
        -- M = 50000000, N = 26 for 1 s
        -- M = 500000000, N = 29 for 10 s
        
        generic (M : integer := 5;  -- generate tick after M clock cycle
                    N : integer := 3); -- -- N bits required to count upto M i.e. 2**N >= M
        port(
            clk, reset: in std_logic;
            clkPulse: out std_logic
        );
    end clockTick;

    architecture arch of clockTick is   
    begin
        -- instantiate Mod-M counter
        clockPulse5cycle: entity work.modMCounter
            generic map (M=>M, N=>N)
            port map (clk=>clk, reset=>reset, complete_tick=>clkPulse);
    end arch;


.. code-block:: vhdl
    :linenos:

    -- modMCounter.vhd

    library ieee; 
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

    entity modMCounter is
        generic (
                M : integer := 5; -- count from 0 to M-1
                N : integer := 3   -- N bits required to count upto M i.e. 2**N >= M
        );
        
        port(
                clk, reset : in std_logic;
                complete_tick : out std_logic;
                count : out std_logic_vector(N-1 downto 0)
        );
    end modMCounter;


    architecture arch of modMCounter is
        signal count_reg, count_next : unsigned(N-1 downto 0);
    begin
        process(clk, reset)
        begin
            if reset = '1' then 
                count_reg <= (others=>'0');
            elsif   clk'event and clk='1' then
                count_reg <= count_next;
            else  -- note that else block is not required
                count_reg <= count_reg;
            end if;
        end process;
        
        -- set count_next to 0 when maximum count is reached i.e. (M-1)
        -- otherwise increase the count
        count_next <= (others=>'0') when count_reg=(M-1) else (count_reg+1);
        
        -- Generate 'tick' on each maximum count
        complete_tick <= '1' when count_reg = (M-1) else '0';
        
        count <= std_logic_vector(count_reg); -- assign value to output port
    end arch;


.. code-block:: text
    :linenos:


    # modMCounter.xdc 

    ## Clock signal 125 MHz
    set_property -dict { PACKAGE_PIN H16   IOSTANDARD LVCMOS33 } [get_ports { CLOCK_50 }]; #IO_L13P_T2_MRCC_35 Sch=sysclk
    # 20 ns clock with 50% duty cycle (i.e. 20/2 = 10)
    create_clock -add -name CLOCK_50 -period 20 -waveform {0 10} [get_ports { CLOCK_50 }];

    # reset
    set_property -dict { PACKAGE_PIN D19   IOSTANDARD LVCMOS33 } [get_ports { reset }]; #IO_L4P_T0_35 Sch=btn[0]


    #LEDG
    set_property -dict { PACKAGE_PIN R14   IOSTANDARD LVCMOS33 } [get_ports { LEDG[0] }]; #IO_L6N_T0_VREF_34 Sch=led[0]
    set_property -dict { PACKAGE_PIN P14   IOSTANDARD LVCMOS33 } [get_ports { LEDG[1] }]; #IO_L6P_T0_34 Sch=led[1]
    set_property -dict { PACKAGE_PIN N16   IOSTANDARD LVCMOS33 } [get_ports { LEDG[2] }]; #IO_L21N_T3_DQS_AD14N_35 Sch=led[2]
    set_property -dict { PACKAGE_PIN M14   IOSTANDARD LVCMOS33 } [get_ports { LEDG[3] }]; #IO_L23P_T3_35 Sch=led[3]


    ##RGB LEDs
    # LEDR : 1 for r part, g and b are 0
    #set_property -dict { PACKAGE_PIN L15   IOSTANDARD LVCMOS33 } [get_ports { led4_b }]; #IO_L22N_T3_AD7N_35 Sch=led4_b
    #set_property -dict { PACKAGE_PIN G17   IOSTANDARD LVCMOS33 } [get_ports { led4_g }]; #IO_L16P_T2_35 Sch=led4_g
    set_property -dict { PACKAGE_PIN N15   IOSTANDARD LVCMOS33 } [get_ports { LEDR[1] }]; #IO_L21P_T3_DQS_AD14P_35 Sch=led4_r
    #set_property -dict { PACKAGE_PIN G14   IOSTANDARD LVCMOS33 } [get_ports { led5_b }]; #IO_0_35 Sch=led5_b
    #set_property -dict { PACKAGE_PIN L14   IOSTANDARD LVCMOS33 } [get_ports { led5_g }]; #IO_L22P_T3_AD7P_35 Sch=led5_g
    set_property -dict { PACKAGE_PIN M15   IOSTANDARD LVCMOS33 } [get_ports { LEDR[0] }]; #IO_L23N_T3_35 Sch=led5_r



Error in Mod-M counter
----------------------


.. error:: 

    * Counting is incorrect when we change the LEDR[1:0] to LEDR and xdc file in above section. Changes are shown in below two codes,  
    * Counting is 0, 3, 6, 9, 0 ... 


.. code-block:: vhdl
    :linenos:

    -- modMCounter_VisualTest.vhd

    library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

    entity modMCounter_VisualTest is 
        generic (
                M : integer := 12; -- count from 0 to M-1
                N : integer := 4   -- N bits required to count upto M i.e. 2**N >= M
        );
        port(
            CLOCK_50, reset: in std_logic;
            LEDR : out std_logic;
            LEDG : out std_logic_vector(N-1 downto 0)
        );
    end modMCounter_VisualTest;

    architecture arch of modMCounter_VisualTest is
        signal clk_Pulse1s: std_logic;
        signal count : std_logic_vector(N-1 downto 0);
    begin
        -- clock 1 s
        clock_1s: entity work.clockTick
        generic map (M=>50000000, N=>26)
        port map (clk=>CLOCK_50, reset=>reset, 
                        clkPulse=>clk_Pulse1s);
                        
    --    LEDR(0) <= clk_Pulse1s; -- display clock pulse of 1 s
        
        -- modMCounter with 1 sec clock pulse
        modMCounter1s: entity work.modMCounter
        generic map (M=>M, N=>N)
        port map (clk=>clk_Pulse1s, reset=>reset, 
                        complete_tick=>LEDR,count=>count);
                        
        LEDG <= count; -- display count on green LEDs
    end;


.. code-block:: text
    :linenos:

    # modMCounter.xdc

    ## Clock signal 125 MHz
    set_property -dict { PACKAGE_PIN H16   IOSTANDARD LVCMOS33 } [get_ports { CLOCK_50 }]; #IO_L13P_T2_MRCC_35 Sch=sysclk
    # 20 ns clock with 50% duty cycle (i.e. 20/2 = 10)
    create_clock -add -name CLOCK_50 -period 20 -waveform {0 10} [get_ports { CLOCK_50 }];

    # reset
    set_property -dict { PACKAGE_PIN D19   IOSTANDARD LVCMOS33 } [get_ports { reset }]; #IO_L4P_T0_35 Sch=btn[0]


    #LEDG
    set_property -dict { PACKAGE_PIN R14   IOSTANDARD LVCMOS33 } [get_ports { LEDG[0] }]; #IO_L6N_T0_VREF_34 Sch=led[0]
    set_property -dict { PACKAGE_PIN P14   IOSTANDARD LVCMOS33 } [get_ports { LEDG[1] }]; #IO_L6P_T0_34 Sch=led[1]
    set_property -dict { PACKAGE_PIN N16   IOSTANDARD LVCMOS33 } [get_ports { LEDG[2] }]; #IO_L21N_T3_DQS_AD14N_35 Sch=led[2]
    set_property -dict { PACKAGE_PIN M14   IOSTANDARD LVCMOS33 } [get_ports { LEDG[3] }]; #IO_L23P_T3_35 Sch=led[3]


    ##RGB LEDs
    # LEDR : 1 for r part, g and b are 0
    #set_property -dict { PACKAGE_PIN L15   IOSTANDARD LVCMOS33 } [get_ports { led4_b }]; #IO_L22N_T3_AD7N_35 Sch=led4_b
    #set_property -dict { PACKAGE_PIN G17   IOSTANDARD LVCMOS33 } [get_ports { led4_g }]; #IO_L16P_T2_35 Sch=led4_g
    set_property -dict { PACKAGE_PIN N15   IOSTANDARD LVCMOS33 } [get_ports { LEDR }]; #IO_L21P_T3_DQS_AD14P_35 Sch=led4_r
    #set_property -dict { PACKAGE_PIN G14   IOSTANDARD LVCMOS33 } [get_ports { led5_b }]; #IO_0_35 Sch=led5_b
    set_property -dict { PACKAGE_PIN L14   IOSTANDARD LVCMOS33 } [get_ports { LEDR }]; #IO_L22P_T3_AD7P_35 Sch=led5_g
    #set_property -dict { PACKAGE_PIN M15   IOSTANDARD LVCMOS33 } [get_ports { LEDR[0] }]; #IO_L23N_T3_35 Sch=led5_r






GPIO LED with Petalinux
=======================



.. code-block:: tcl

    create_project zynq_gpio ./zynq_gpio -part xc7z020clg400-1
    set_property board_part www.digilentinc.com:pynq-z1:part0:1.0 [current_project]


    create_bd_design "design_1"
    update_compile_order -fileset sources_1


    # Zynq
    create_bd_cell -type ip -vlnv xilinx.com:ip:processing_system7:5.5 processing_system7_0
    apply_bd_automation -rule xilinx.com:bd_rule:processing_system7 -config {make_external "FIXED_IO, DDR" apply_board_preset "1" Master "Disable" Slave "Disable" }  [get_bd_cells processing_system7_0]

    # GPIO for buttons
    create_bd_cell -type ip -vlnv xilinx.com:ip:axi_gpio:2.0 axi_gpio_0
    set_property name button_4bits [get_bd_cells axi_gpio_0]
    set_property -dict [list CONFIG.C_GPIO_WIDTH {4} CONFIG.C_GPIO2_WIDTH {4} CONFIG.C_IS_DUAL {0} CONFIG.C_ALL_INPUTS {1} CONFIG.C_ALL_INPUTS_2 {1} CONFIG.GPIO_BOARD_INTERFACE {btns_4bits} CONFIG.GPIO2_BOARD_INTERFACE {Custom}] [get_bd_cells button_4bits]
    apply_bd_automation -rule xilinx.com:bd_rule:axi4 -config { Clk_master {Auto} Clk_slave {Auto} Clk_xbar {Auto} Master {/processing_system7_0/M_AXI_GP0} Slave {/button_4bits/S_AXI} ddr_seg {Auto} intc_ip {New AXI Interconnect} master_apm {0}}  [get_bd_intf_pins button_4bits/S_AXI]


    # GPIO for switch
    create_bd_cell -type ip -vlnv xilinx.com:ip:axi_gpio:2.0 axi_gpio_0
    set_property name sw_2bits [get_bd_cells axi_gpio_0]
    set_property -dict [list CONFIG.C_GPIO_WIDTH {2} CONFIG.C_IS_DUAL {1} CONFIG.C_ALL_INPUTS {1} CONFIG.GPIO_BOARD_INTERFACE {sws_2bits}] [get_bd_cells sw_2bits]
    set_property -dict [list CONFIG.C_IS_DUAL {0}] [get_bd_cells sw_2bits]
    apply_bd_automation -rule xilinx.com:bd_rule:axi4 -config { Clk_master {/processing_system7_0/FCLK_CLK0 (100 MHz)} Clk_slave {Auto} Clk_xbar {/processing_system7_0/FCLK_CLK0 (100 MHz)} Master {/processing_system7_0/M_AXI_GP0} Slave {/sw_2bits/S_AXI} ddr_seg {Auto} intc_ip {/ps7_0_axi_periph} master_apm {0}}  [get_bd_intf_pins sw_2bits/S_AXI]


    # GPIO for LEDs
    create_bd_cell -type ip -vlnv xilinx.com:ip:axi_gpio:2.0 axi_gpio_0
    set_property name led_4bits [get_bd_cells axi_gpio_0]
    set_property -dict [list CONFIG.C_GPIO_WIDTH {4} CONFIG.C_IS_DUAL {0} CONFIG.GPIO_BOARD_INTERFACE {leds_4bits}] [get_bd_cells led_4bits]
    apply_bd_automation -rule xilinx.com:bd_rule:axi4 -config { Clk_master {/processing_system7_0/FCLK_CLK0 (100 MHz)} Clk_slave {Auto} Clk_xbar {/processing_system7_0/FCLK_CLK0 (100 MHz)} Master {/processing_system7_0/M_AXI_GP0} Slave {/led_4bits/S_AXI} ddr_seg {Auto} intc_ip {/ps7_0_axi_periph} master_apm {0}}  [get_bd_intf_pins led_4bits/S_AXI]


    make_bd_intf_pins_external  [get_bd_intf_pins button_4bits/GPIO]
    set_property name button_4bits [get_bd_intf_ports GPIO_0]
    make_bd_intf_pins_external  [get_bd_intf_pins sw_2bits/GPIO]
    set_property name sw_2bits [get_bd_intf_ports GPIO_0]
    make_bd_intf_pins_external  [get_bd_intf_pins led_4bits/GPIO]
    set_property name led_4bits [get_bd_intf_ports GPIO_0]


    validate_bd_design
    regenerate_bd_layout
    save_bd_design

    generate_target all [get_files  ./zynq_gpio/zynq_gpio.srcs/sources_1/bd/design_1/design_1.bd]
    make_wrapper -files [get_files ./zynq_gpio/zynq_gpio.srcs/sources_1/bd/design_1/design_1.bd] -top
    add_files -norecurse ./zynq_gpio/zynq_gpio.srcs/sources_1/bd/design_1/hdl/design_1_wrapper.v
    launch_runs impl_1 -to_step write_bitstream -jobs 56


    write_hw_platform -fixed -force  -include_bit -file ./zynq_gpio/design_1_wrapper.xsa



.. code-block:: shell

    petalinux-create -t project --name gpio_test --template zynq
    cp ./zynq_gpio/design_1_wrapper.xsa xsa/
    petalinux-config --get-hw-description=./xsa/
    petalinux-config -c kernel 
    petalinux-config -c rootfs
    petalinux-build
    cd images/linux/
    petalinux-package --boot --fsbl zynq_fsbl.elf --fpga system.bit --u-boot 




Copy BOOT.bin and image.ub in sd card and boot the fpga. 

.. code-block:: text

    root@gpio_test:~# ls -la  /sys/class/gpio
    drwxr-xr-x    2 root     root             0 Jan  1  1970 .
    drwxr-xr-x   44 root     root             0 Jan  1  1970 ..
    --w-------    1 root     root          4096 Jan  1  1970 export
    lrwxrwxrwx    1 root     root             0 Jan  1  1970 gpiochip1014 -> ../../devices/soc0/amba_pl/41210000.gpio/gpio/gpiochip1014
    lrwxrwxrwx    1 root     root             0 Jan  1  1970 gpiochip1016 -> ../../devices/soc0/amba_pl/41220000.gpio/gpio/gpiochip1016
    lrwxrwxrwx    1 root     root             0 Jan  1  1970 gpiochip1020 -> ../../devices/soc0/amba_pl/41200000.gpio/gpio/gpiochip1020
    lrwxrwxrwx    1 root     root             0 Jan  1  1970 gpiochip896 -> ../../devices/soc0/amba/e000a000.gpio/gpio/gpiochip896
    --w-------    1 root     root          4096 Jan  1  1970 unexport


    (glow LED 0)
    root@gpio_test:~# gpio-demo -g 1016 -o 1
    (glow led 1)
    root@gpio_test:~# gpio-demo -g 1016 -o 0x2
    (glow led 0 and 1)
    root@gpio_test:~# gpio-demo -g 1016 -o 0x3
    (glow all led)
    root@gpio_test:~# gpio-demo -g 1016 -o 0xf  